Cabify code exercise
====================

##### Besides providing exceptional transportation services, Cabify also runs a physical store which sells (only) 3 products:

``` 
Code         | Name                |  Price
-------------------------------------------------
VOUCHER      | Cabify Voucher      |   5.00€
TSHIRT       | Cabify T-Shirt      |  20.00€
MUG          | Cafify Coffee Mug   |   7.50€
```

Various departments have insisted on the following discounts:

 * The marketing department believes in 2-for-1 promotions (buy two of the same product, get one free), and would like for there to be a 2-for-1 special on `VOUCHER` items.

 * The CFO insists that the best way to increase sales is with discounts on bulk purchases (buying x or more of a product, the price of that product is reduced), and demands that if you buy 3 or more `TSHIRT` items, the price per unit should be 19.00€.

Cabify's checkout process allows for items to be scanned in any order, and should return the total amount to be paid. The interface for the checkout process looks like this (ruby):

```ruby
co = Checkout.new(pricing_rules)
co.scan("VOUCHER")
co.scan("VOUCHER")
co.scan("TSHIRT")
price = co.total
```

Using ruby (>= 2.0), implement a checkout process that fulfills the requirements.

Examples:

    Items: VOUCHER, TSHIRT, MUG
    Total: 32.50€

    Items: VOUCHER, TSHIRT, VOUCHER
    Total: 25.00€

    Items: TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT
    Total: 81.00€

    Items: VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT
    Total: 74.50€

**The code should:**
- Be written as production-ready code. You will write production code.
- Be easy to grow and easy to add new functionality.
- Have notes attached, explaning the solution and why certain things are included and others are left out.

## Execute exercise examples

```ruby
ruby store.rb
```

## How to run tests

```ruby
bundle install
bundle exec rspec
```

## Explanation

Code validated from the beginning using a TDD integration test and built the solution from it. Even the unit tests are peding to add the code could be considered production ready.

The code use some patterns to allow the growth in functionality, avoiding too much overengineering. For example adding a new product should be as easy as add it following the structure in `assets/products.yml`. Also add a new PricingRule only implies add a new file in `lib/pricing_rules/` using the same structure.


### Included and Left

* Left DB persistance of Product and Checkout. The exercide didn't require it, and decided to focus on solve the exercise without expending time on not required things. Anyway, add db persistance to the existing code would be very easy because the code is quite isolated.
* Included integration test with Minitest to validate the implementation from the beginning then moved to Rspec for more comples tests
* Included some SOLID principles as SRP or OCP.
* Left currency definition, the solution assume everything would use the same currency.
* Included an easy way to include new products to the existing solution
* Included an easy way to include new Pricing Rules to the existing solution
* Included bundler to ensure the code is executed with the same gem versions.

