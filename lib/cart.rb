require_relative 'cart_item'

class Cart
  def initialize
    @list = {}
  end

  def add(product)
    cart_item = find_product(product)

    if cart_item
      cart_item.increment
    else
      list[product.code] = CartItem.new(product)
    end
  end

  def to_a
    list.values
  end

  private
  attr_reader :list

  def find_product(product)
    list[product.code]
  end
end
