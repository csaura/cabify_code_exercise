require_relative 'base'

module PricingRules
  class DefaultRule < Base
    def self.elegible_for?(_cart_item)
      false
    end

    def self.calculate(cart_item)
      cart_item.unit_price * cart_item.quantity
    end
  end
end
