module PricingRules
  class Base
    def self.inherited(base)
      PricingRules.register(base)
    end

    def self.elegible_for?(_product_line)
      raise NotImplementedError
    end

    def self.calculate(_product_line)
      raise NotImplementedError
    end
  end
end
