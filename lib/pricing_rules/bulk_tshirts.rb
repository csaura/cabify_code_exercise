require_relative 'base'

module PricingRules
  class BulkTshirts < Base
    CODE = 'TSHIRT'.freeze
    MIN_QUANTITY = 3
    FIXED_PRICE = 19.0

    def self.elegible_for?(cart_item)
      CODE == cart_item.code && MIN_QUANTITY <= cart_item.quantity
    end

    def self.calculate(cart_item)
      FIXED_PRICE * cart_item.quantity
    end
  end
end
