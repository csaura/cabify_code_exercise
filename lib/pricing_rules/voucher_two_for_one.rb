require_relative 'base'

module PricingRules
  class VoucherTwoForOne < Base
    CODE = 'VOUCHER'.freeze

    def self.elegible_for?(cart_item)
      CODE == cart_item.code
    end

    def self.calculate(cart_item)
      quantity = cart_item.quantity
      calculated_quantity = quantity/2
      calculated_quantity += 1 if quantity.odd?
      cart_item.unit_price * calculated_quantity
    end
  end
end
