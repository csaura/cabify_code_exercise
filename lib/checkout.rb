require_relative 'products'
require_relative 'pricing_rules'
require_relative 'cart'

class Checkout
  class InvalidProduct < StandardError; end

  def initialize(pricing_rules)
    @pricing_rules = pricing_rules
    @cart = Cart.new
  end

  def scan(product_code)
    product = Products.find_by_code(product_code)
    raise InvalidProduct, "Product with code #{product_code} not found" unless product
    cart.add(product)
  end

  def total
    cart.to_a.map { |cart_item| pricing_rules.apply(cart_item) }.reduce(0, :+)
  end

  private

  attr_reader :pricing_rules
  attr_accessor :cart
end
