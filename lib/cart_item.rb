class CartItem
  attr_reader :code, :name, :unit_price, :quantity

  def initialize(product)
    @code = product.code
    @name = product.name
    @unit_price = product.price
    @quantity = 1
  end

  def increment
    @quantity += 1
  end
end
