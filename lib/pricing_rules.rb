require 'set'

module PricingRules
  def rules
    @rules ||= Set.new
  end

  def register(rule)
    rules << rule
  end

  # This only consider one elegible pricing rule for cart_item
  # and only takes the first pricing_rule.
  #
  # In a future development, if is required, a pricing_rule comparator could be
  # implemented for cases that a product line is elegible for many rules. This
  # comparator could take the most profitable pricing rule for the customer.
  def apply(cart_item)
    pricing_rule = rules.find{ |rule| rule.elegible_for?(cart_item) }
    pricing_rule ||= DefaultRule
    pricing_rule.calculate(cart_item)
  end

  module_function :rules
  module_function :register
  module_function :apply

  class << self
    private :rules
  end
end

# Load all pricing rules following the open close principe
# To add a new pricing rule only is needed to add it to the folder
# and inherit from PricingRules::Base
#
# require 'pricing_rules/base'
# require 'pricing_rules/bulk_tshirts'
# require 'pricing_rules/default_rule'
# require 'pricing_rules/voucher_two_for_one'
Dir[File.dirname(__FILE__) + '/pricing_rules/**/*.rb'].each { |file| require file }
