require 'yaml'
require_relative 'product'

module Products
  extend self

  class ProductsRepository

    def self.instance
      @__instance__ ||= new
    end

    attr_reader :products

    def initialize
      yaml_file = YAML.load_file(File.expand_path('../../assets/products.yml', __FILE__))
      @products = yaml_file['products'].map {|_key, values|  Product.new(values) }
    end

    private_class_method :new
  end
  private_constant :ProductsRepository

  def find_by_code(product_code)
    ProductsRepository.instance.products.find {|product| product.code == product_code}
  end
end

