class Product
  attr_reader :code, :name, :price

  def initialize(values)
    @code = values['code']
    @name = values['name']
    @price = values['price']
  end
end
