require 'spec_helper'
require_relative '../../lib/pricing_rules/voucher_two_for_one'
require_relative '../../lib/cart_item'
require_relative '../../lib/products'

describe PricingRules::BulkTshirts do
  describe '#elegible_for?' do
    subject { described_class.elegible_for?(cart_item) }

    context 'when the cart_item is not elegible because are not TSHIRTS' do
      let(:cart_item) do
        CartItem.new(Products.find_by_code('MUG'))
      end
      it { is_expected.to be(false)}
    end

    context 'when the cart_item is not elegible because needs at least 3 tshirts' do
      let(:cart_item) do
        item = CartItem.new(Products.find_by_code('TSHIRT'))
        item.increment
        item
      end
      it { is_expected.to be(false)}
    end

    context 'when the cart_item is elegible because are TSHIRTS and the quantity is 3' do
      let(:cart_item) do
        item = CartItem.new(Products.find_by_code('TSHIRT'))
        item.increment
        item.increment
        item
      end
      it { is_expected.to be(true)}
    end

    context 'when the cart_item is elegible because are TSHIRTS and the quantity more than 3' do
      let(:cart_item) do
        item = CartItem.new(Products.find_by_code('TSHIRT'))
        10.times { item.increment }
        item
      end
      it { is_expected.to be(true)}
    end
  end

  describe '#calculate' do
    subject { described_class.calculate(cart_item) }

    let(:cart_item) do
      item = CartItem.new(Products.find_by_code('TSHIRT'))
      item.increment
      item.increment
      item
    end

    it { is_expected.to eq(57.0)}
  end
end
