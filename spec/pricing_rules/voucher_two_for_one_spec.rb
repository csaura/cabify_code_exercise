require 'spec_helper'
require_relative '../../lib/pricing_rules/voucher_two_for_one'
require_relative '../../lib/cart_item'
require_relative '../../lib/products'

describe PricingRules::VoucherTwoForOne do
  describe '#elegible_for?' do
    subject { described_class.elegible_for?(cart_item) }

    context 'when the cart_item is not elegible' do
      let(:cart_item) do
        CartItem.new(Products.find_by_code('MUG'))
      end
      it { is_expected.to be(false)}
    end

    context 'when the cart_item is elegible' do
      let(:cart_item) do
        item = CartItem.new(Products.find_by_code('VOUCHER'))
        item.increment
        item
      end
      it { is_expected.to be(true)}
    end
  end

  describe '#calculate' do
    subject { described_class.calculate(cart_item) }

    context 'is applied for tuples of items' do
      let(:cart_item) do
        item = CartItem.new(Products.find_by_code('VOUCHER'))
        item.increment
        item
      end

      it { is_expected.to eq(5.0)}
    end

    context 'is applied for tuples of items but not to odd number of items' do
      let(:cart_item) do
        item = CartItem.new(Products.find_by_code('VOUCHER'))
        item.increment
        item.increment
        item
      end

      it { is_expected.to eq(10.0)}
    end
  end
end
