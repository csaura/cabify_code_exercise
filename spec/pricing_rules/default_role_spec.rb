require 'spec_helper'
require_relative '../../lib/pricing_rules/voucher_two_for_one'
require_relative '../../lib/cart_item'
require_relative '../../lib/products'

describe PricingRules::DefaultRule do
  describe '#elegible_for?' do
    subject { described_class.elegible_for?(cart_item) }
    let(:cart_item) { CartItem.new(Products.find_by_code('MUG')) }
    it { is_expected.to be(false)}
  end

  describe '#calculate' do
    subject { described_class.calculate(cart_item) }

    let(:cart_item) do
      item = CartItem.new(Products.find_by_code('MUG'))
      item.increment
      item
    end

    it { is_expected.to eq(15.0)}
  end
end
