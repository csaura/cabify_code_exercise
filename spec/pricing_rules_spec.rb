require 'spec_helper'
require_relative '../lib/pricing_rules'
require_relative '../lib/cart_item'
require_relative '../lib/products'

describe PricingRules do
  describe '#apply' do
    context 'do not find an elegible pricing rule' do
      let(:cart_item) { CartItem.new(Products.find_by_code('MUG'))}
      it 'call DefaultRule' do
        expect(PricingRules::DefaultRule).to receive(:calculate).with(cart_item)
        PricingRules.apply(cart_item)
      end
    end

    context 'do find an elegible pricing rule' do
      let(:cart_item) do
        item = CartItem.new(Products.find_by_code('VOUCHER'))
        item.increment
        item
      end

      it 'call the pricing rule' do
        expect(PricingRules::VoucherTwoForOne).to receive(:calculate).with(cart_item)
        PricingRules.apply(cart_item)
      end
    end
  end
end
