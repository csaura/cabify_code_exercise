require 'spec_helper'
require_relative '../lib/product'

describe Product do
  let(:hash_values) { { 'code' => 'MUG', 'name' => 'Cabify Mug', 'price' => 7.5 } }
  let(:product) { Product.new(hash_values) }

  describe '#new' do
    it 'set code' do
      expect(product.code).to eq('MUG')
    end

    it 'set name' do
      expect(product.name).to eq('Cabify Mug')
    end

    it 'set price' do
      expect(product.price).to eq(7.5)
    end
  end
end
