require 'spec_helper'
require_relative '../lib/cart_item'
require_relative '../lib/products'

describe CartItem do
  let(:cart_item) { CartItem.new(mug) }
  let(:mug) { Products.find_by_code('MUG') }

  describe '#new' do
    it 'use the product code' do
      expect(cart_item.code).to eq(mug.code)
    end

    it 'use the product name' do
      expect(cart_item.name).to eq(mug.name)
    end

    it 'use the product price as unit_price' do
      expect(cart_item.unit_price).to eq(mug.price)
    end

    it 'the default quantity is 1' do
      expect(cart_item.quantity).to eq(1)
    end
  end

  describe '#increment' do
    it 'add 1 item to the quantity' do
      expect { cart_item.increment }.to change { cart_item.quantity }.by(1)
    end
  end
end
