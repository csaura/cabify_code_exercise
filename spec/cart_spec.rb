require 'spec_helper'
require_relative '../lib/cart'

describe Cart do
  let(:cart) { Cart.new }
  let(:mug) { Products.find_by_code('MUG') }
  let(:tshirt) { Products.find_by_code('TSHIRT') }

  describe '#add' do
    context 'when the cart was empty' do
      it 'add new item' do
        expect { cart.add(mug) }.to change { cart.to_a.size }.from(0).to(1)
      end

      it 'the element is added' do
        cart.add(mug)
        expect(cart.to_a.map(&:code)).to eq(['MUG'])
      end

      it 'the quantity is 1' do
        cart.add(mug)
        expect(cart.to_a.map(&:quantity)).to eq([1])
      end
    end

    context 'when the cart has items of the same product' do
      before do
        cart.add(mug)
      end

      it 'do not add new item' do
        expect { cart.add(mug) }.to_not change { cart.to_a.size }
      end

      it 'the element still added' do
        cart.add(mug)
        expect(cart.to_a.map(&:code)).to eq(['MUG'])
      end

      it 'the quantity is incremented' do
        cart.add(mug)
        expect(cart.to_a.map(&:quantity)).to eq([2])
      end
    end

    context 'when the cart has items of other product' do
      before do
        cart.add(mug)
      end

      it 'add new item' do
        expect { cart.add(tshirt) }.to change { cart.to_a.size }.by(1)
      end

      it 'the element is added' do
        cart.add(tshirt)
        expect(cart.to_a.map(&:code)).to eq(['MUG', 'TSHIRT'])
      end

      it 'the quantity is 1' do
        cart.add(tshirt)
        expect(cart.to_a.map(&:quantity)).to eq([1, 1])
      end
    end
  end

  describe '#to_a' do
    subject { cart.to_a }
    context 'when the cart is empty' do
      it { is_expected.to eq([]) }
    end

    context 'when the cart has items' do
      before do
        cart.add(mug)
      end

      it { expect(subject.map(&:code)).to eq(['MUG']) }
    end
  end
end
