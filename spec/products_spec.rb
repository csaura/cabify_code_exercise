require 'spec_helper'
require_relative '../lib/products'

describe Products do
  describe '#find_by_code' do
    it 'returns the product with the given code' do
      expect(Products.find_by_code('MUG').code).to eq('MUG')
    end

    it 'returns nil when the given code does not match with a product' do
      expect(Products.find_by_code('HOODIE')).to be(nil)
    end
  end
end
