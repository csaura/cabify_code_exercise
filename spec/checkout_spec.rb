require 'spec_helper'
require_relative '../lib/checkout'

describe Checkout do
  let(:checkout) { Checkout.new(PricingRules) }

  describe '#total' do
    subject { checkout.total }

    context 'checkout 1' do
      before do
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        checkout.scan('MUG')
      end

      it { is_expected.to eq(32.50) }
    end

    context 'checkout 2' do
      before do
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        checkout.scan('VOUCHER')
      end

      it { is_expected.to eq(25.00) }
    end

    context 'checkout 3' do
      before do
        checkout.scan('TSHIRT')
        checkout.scan('TSHIRT')
        checkout.scan('TSHIRT')
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
      end

      it { is_expected.to eq(81.00) }
    end

    context 'checkout 4' do
      before do
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        checkout.scan('VOUCHER')
        checkout.scan('VOUCHER')
        checkout.scan('MUG')
        checkout.scan('TSHIRT')
        checkout.scan('TSHIRT')
      end

      it { is_expected.to eq(74.50) }
    end
  end

  describe '#scan' do
    subject { checkout.scan(product_code) }

    context 'when the product do not exists' do
      let(:product_code) { 'HOODIE' }

      it 'raises an exception'do
        expect { subject }.to raise_error(Checkout::InvalidProduct, 'Product with code HOODIE not found')
      end
    end

    context 'when the product do not exists' do
      let(:product_code) { 'MUG' }
      it { is_expected.to be_truthy }
    end
  end
end
