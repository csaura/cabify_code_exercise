# coding: utf-8
require_relative 'lib/checkout'

separator = '=' * 70
puts separator
puts separator
puts 'Execise store examples'
puts separator
puts separator

puts 'Items: VOUCHER, TSHIRT, MUG'
checkout = Checkout.new(PricingRules)
checkout.scan('VOUCHER')
checkout.scan('TSHIRT')
checkout.scan('MUG')
puts 'Total: %.2f€' % checkout.total
puts separator

puts 'Items: VOUCHER, TSHIRT, VOUCHER'
checkout = Checkout.new(PricingRules)
checkout.scan('VOUCHER')
checkout.scan('TSHIRT')
checkout.scan('VOUCHER')
puts 'Total: %.2f€' % checkout.total
puts separator

puts 'Items: TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT'
checkout = Checkout.new(PricingRules)
checkout.scan('TSHIRT')
checkout.scan('TSHIRT')
checkout.scan('TSHIRT')
checkout.scan('VOUCHER')
checkout.scan('TSHIRT')
puts 'Total: %.2f€' % checkout.total
puts separator

puts 'Items: VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT'
checkout = Checkout.new(PricingRules)
checkout.scan('VOUCHER')
checkout.scan('TSHIRT')
checkout.scan('VOUCHER')
checkout.scan('VOUCHER')
checkout.scan('MUG')
checkout.scan('TSHIRT')
checkout.scan('TSHIRT')
puts 'Total: %.2f€' % checkout.total
puts separator

puts 'DONE!'
